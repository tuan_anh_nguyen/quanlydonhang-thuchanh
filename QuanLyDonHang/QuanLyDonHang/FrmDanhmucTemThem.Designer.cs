﻿namespace Quanlydonhang
{
    partial class FrmDanhmucTemThem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucTemThem));
            this.btnLuuthemtem = new System.Windows.Forms.Button();
            this.btnThoatthemtem = new System.Windows.Forms.Button();
            this.lblThemthongtintem = new System.Windows.Forms.Label();
            this.lblDuongke = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.tbxThongsokythuattem = new System.Windows.Forms.TextBox();
            this.tbxTentem = new System.Windows.Forms.TextBox();
            this.tbxMatem = new System.Windows.Forms.TextBox();
            this.lblThongsokythuat = new System.Windows.Forms.Label();
            this.lblDonvitinh = new System.Windows.Forms.Label();
            this.lblTentem = new System.Windows.Forms.Label();
            this.tbxDonvitinhtem = new System.Windows.Forms.TextBox();
            this.lblMatem = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnLuuthemtem
            // 
            this.btnLuuthemtem.Location = new System.Drawing.Point(561, 19);
            this.btnLuuthemtem.Margin = new System.Windows.Forms.Padding(2);
            this.btnLuuthemtem.Name = "btnLuuthemtem";
            this.btnLuuthemtem.Size = new System.Drawing.Size(134, 28);
            this.btnLuuthemtem.TabIndex = 17;
            this.btnLuuthemtem.Text = "Lưu";
            this.btnLuuthemtem.UseVisualStyleBackColor = true;
            // 
            // btnThoatthemtem
            // 
            this.btnThoatthemtem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoatthemtem.Location = new System.Drawing.Point(713, 19);
            this.btnThoatthemtem.Margin = new System.Windows.Forms.Padding(2);
            this.btnThoatthemtem.Name = "btnThoatthemtem";
            this.btnThoatthemtem.Size = new System.Drawing.Size(134, 28);
            this.btnThoatthemtem.TabIndex = 19;
            this.btnThoatthemtem.Text = "Thoát";
            this.btnThoatthemtem.UseVisualStyleBackColor = true;
            // 
            // lblThemthongtintem
            // 
            this.lblThemthongtintem.AutoSize = true;
            this.lblThemthongtintem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThemthongtintem.Location = new System.Drawing.Point(7, 19);
            this.lblThemthongtintem.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblThemthongtintem.Name = "lblThemthongtintem";
            this.lblThemthongtintem.Size = new System.Drawing.Size(199, 20);
            this.lblThemthongtintem.TabIndex = 20;
            this.lblThemthongtintem.Text = "THÊM THÔNG TIN TEM";
            // 
            // lblDuongke
            // 
            this.lblDuongke.AutoSize = true;
            this.lblDuongke.ForeColor = System.Drawing.Color.DarkGray;
            this.lblDuongke.Location = new System.Drawing.Point(8, 40);
            this.lblDuongke.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDuongke.Name = "lblDuongke";
            this.lblDuongke.Size = new System.Drawing.Size(865, 13);
            this.lblDuongke.TabIndex = 18;
            this.lblDuongke.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tbxThongsokythuattem
            // 
            this.tbxThongsokythuattem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxThongsokythuattem.Location = new System.Drawing.Point(268, 87);
            this.tbxThongsokythuattem.Multiline = true;
            this.tbxThongsokythuattem.Name = "tbxThongsokythuattem";
            this.tbxThongsokythuattem.Size = new System.Drawing.Size(288, 96);
            this.tbxThongsokythuattem.TabIndex = 40;
            // 
            // tbxTentem
            // 
            this.tbxTentem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxTentem.Location = new System.Drawing.Point(15, 163);
            this.tbxTentem.Name = "tbxTentem";
            this.tbxTentem.Size = new System.Drawing.Size(193, 20);
            this.tbxTentem.TabIndex = 38;
            // 
            // tbxMatem
            // 
            this.tbxMatem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxMatem.Location = new System.Drawing.Point(15, 87);
            this.tbxMatem.Name = "tbxMatem";
            this.tbxMatem.Size = new System.Drawing.Size(193, 20);
            this.tbxMatem.TabIndex = 37;
            // 
            // lblThongsokythuat
            // 
            this.lblThongsokythuat.AutoSize = true;
            this.lblThongsokythuat.Location = new System.Drawing.Point(265, 71);
            this.lblThongsokythuat.Name = "lblThongsokythuat";
            this.lblThongsokythuat.Size = new System.Drawing.Size(93, 13);
            this.lblThongsokythuat.TabIndex = 44;
            this.lblThongsokythuat.Text = "Thông số kỹ thuật";
            // 
            // lblDonvitinh
            // 
            this.lblDonvitinh.AutoSize = true;
            this.lblDonvitinh.Location = new System.Drawing.Point(13, 223);
            this.lblDonvitinh.Name = "lblDonvitinh";
            this.lblDonvitinh.Size = new System.Drawing.Size(60, 13);
            this.lblDonvitinh.TabIndex = 43;
            this.lblDonvitinh.Text = "Đơn vị tính";
            // 
            // lblTentem
            // 
            this.lblTentem.AutoSize = true;
            this.lblTentem.Location = new System.Drawing.Point(13, 147);
            this.lblTentem.Name = "lblTentem";
            this.lblTentem.Size = new System.Drawing.Size(46, 13);
            this.lblTentem.TabIndex = 42;
            this.lblTentem.Text = "Tên tem";
            // 
            // tbxDonvitinhtem
            // 
            this.tbxDonvitinhtem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxDonvitinhtem.Location = new System.Drawing.Point(15, 239);
            this.tbxDonvitinhtem.Name = "tbxDonvitinhtem";
            this.tbxDonvitinhtem.Size = new System.Drawing.Size(193, 20);
            this.tbxDonvitinhtem.TabIndex = 39;
            // 
            // lblMatem
            // 
            this.lblMatem.AutoSize = true;
            this.lblMatem.Location = new System.Drawing.Point(13, 71);
            this.lblMatem.Name = "lblMatem";
            this.lblMatem.Size = new System.Drawing.Size(42, 13);
            this.lblMatem.TabIndex = 41;
            this.lblMatem.Text = "Mã tem";
            // 
            // FrmDanhmucTemThem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.tbxThongsokythuattem);
            this.Controls.Add(this.tbxTentem);
            this.Controls.Add(this.tbxMatem);
            this.Controls.Add(this.lblThongsokythuat);
            this.Controls.Add(this.lblDonvitinh);
            this.Controls.Add(this.lblTentem);
            this.Controls.Add(this.tbxDonvitinhtem);
            this.Controls.Add(this.lblMatem);
            this.Controls.Add(this.btnLuuthemtem);
            this.Controls.Add(this.btnThoatthemtem);
            this.Controls.Add(this.lblThemthongtintem);
            this.Controls.Add(this.lblDuongke);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucTemThem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucTemThem";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnLuuthemtem;
        private System.Windows.Forms.Button btnThoatthemtem;
        private System.Windows.Forms.Label lblThemthongtintem;
        private System.Windows.Forms.Label lblDuongke;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.TextBox tbxThongsokythuattem;
        private System.Windows.Forms.TextBox tbxTentem;
        private System.Windows.Forms.TextBox tbxMatem;
        private System.Windows.Forms.Label lblThongsokythuat;
        private System.Windows.Forms.Label lblDonvitinh;
        private System.Windows.Forms.Label lblTentem;
        private System.Windows.Forms.TextBox tbxDonvitinhtem;
        private System.Windows.Forms.Label lblMatem;
    }
}