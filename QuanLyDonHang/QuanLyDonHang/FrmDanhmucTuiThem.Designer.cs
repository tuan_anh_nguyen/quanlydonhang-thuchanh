﻿namespace Quanlydonhang
{
    partial class FrmDanhmucTuiThem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucTuiThem));
            this.btnThoatthemtui = new System.Windows.Forms.Button();
            this.lblThemthongtintui = new System.Windows.Forms.Label();
            this.lblDuongke = new System.Windows.Forms.Label();
            this.btnLuuthemtui = new System.Windows.Forms.Button();
            this.lblMatui = new System.Windows.Forms.Label();
            this.lblTentui = new System.Windows.Forms.Label();
            this.lblDonvitinh = new System.Windows.Forms.Label();
            this.lblThongsokythuattui = new System.Windows.Forms.Label();
            this.tbxMatui = new System.Windows.Forms.TextBox();
            this.tbxTentui = new System.Windows.Forms.TextBox();
            this.tbxDonvitinhtui = new System.Windows.Forms.TextBox();
            this.tbxThongsokythuattui = new System.Windows.Forms.TextBox();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.SuspendLayout();
            // 
            // btnThoatthemtui
            // 
            this.btnThoatthemtui.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoatthemtui.Location = new System.Drawing.Point(713, 19);
            this.btnThoatthemtui.Margin = new System.Windows.Forms.Padding(2);
            this.btnThoatthemtui.Name = "btnThoatthemtui";
            this.btnThoatthemtui.Size = new System.Drawing.Size(134, 28);
            this.btnThoatthemtui.TabIndex = 6;
            this.btnThoatthemtui.Text = "Thoát";
            this.btnThoatthemtui.UseVisualStyleBackColor = true;
            // 
            // lblThemthongtintui
            // 
            this.lblThemthongtintui.AutoSize = true;
            this.lblThemthongtintui.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThemthongtintui.Location = new System.Drawing.Point(7, 19);
            this.lblThemthongtintui.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblThemthongtintui.Name = "lblThemthongtintui";
            this.lblThemthongtintui.Size = new System.Drawing.Size(192, 20);
            this.lblThemthongtintui.TabIndex = 6;
            this.lblThemthongtintui.Text = "THÊM THÔNG TIN TÚI";
            // 
            // lblDuongke
            // 
            this.lblDuongke.AutoSize = true;
            this.lblDuongke.ForeColor = System.Drawing.Color.DarkGray;
            this.lblDuongke.Location = new System.Drawing.Point(8, 40);
            this.lblDuongke.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDuongke.Name = "lblDuongke";
            this.lblDuongke.Size = new System.Drawing.Size(865, 13);
            this.lblDuongke.TabIndex = 5;
            this.lblDuongke.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // btnLuuthemtui
            // 
            this.btnLuuthemtui.Location = new System.Drawing.Point(561, 19);
            this.btnLuuthemtui.Margin = new System.Windows.Forms.Padding(2);
            this.btnLuuthemtui.Name = "btnLuuthemtui";
            this.btnLuuthemtui.Size = new System.Drawing.Size(134, 28);
            this.btnLuuthemtui.TabIndex = 5;
            this.btnLuuthemtui.Text = "Lưu";
            this.btnLuuthemtui.UseVisualStyleBackColor = true;
            // 
            // lblMatui
            // 
            this.lblMatui.AutoSize = true;
            this.lblMatui.Location = new System.Drawing.Point(13, 71);
            this.lblMatui.Name = "lblMatui";
            this.lblMatui.Size = new System.Drawing.Size(36, 13);
            this.lblMatui.TabIndex = 9;
            this.lblMatui.Text = "Mã túi";
            // 
            // lblTentui
            // 
            this.lblTentui.AutoSize = true;
            this.lblTentui.Location = new System.Drawing.Point(13, 147);
            this.lblTentui.Name = "lblTentui";
            this.lblTentui.Size = new System.Drawing.Size(40, 13);
            this.lblTentui.TabIndex = 10;
            this.lblTentui.Text = "Tên túi";
            // 
            // lblDonvitinh
            // 
            this.lblDonvitinh.AutoSize = true;
            this.lblDonvitinh.Location = new System.Drawing.Point(13, 223);
            this.lblDonvitinh.Name = "lblDonvitinh";
            this.lblDonvitinh.Size = new System.Drawing.Size(60, 13);
            this.lblDonvitinh.TabIndex = 11;
            this.lblDonvitinh.Text = "Đơn vị tính";
            // 
            // lblThongsokythuattui
            // 
            this.lblThongsokythuattui.AutoSize = true;
            this.lblThongsokythuattui.Location = new System.Drawing.Point(265, 71);
            this.lblThongsokythuattui.Name = "lblThongsokythuattui";
            this.lblThongsokythuattui.Size = new System.Drawing.Size(93, 13);
            this.lblThongsokythuattui.TabIndex = 12;
            this.lblThongsokythuattui.Text = "Thông số kỹ thuật";
            // 
            // tbxMatui
            // 
            this.tbxMatui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxMatui.Location = new System.Drawing.Point(15, 87);
            this.tbxMatui.Name = "tbxMatui";
            this.tbxMatui.Size = new System.Drawing.Size(193, 20);
            this.tbxMatui.TabIndex = 1;
            // 
            // tbxTentui
            // 
            this.tbxTentui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxTentui.Location = new System.Drawing.Point(15, 163);
            this.tbxTentui.Name = "tbxTentui";
            this.tbxTentui.Size = new System.Drawing.Size(193, 20);
            this.tbxTentui.TabIndex = 2;
            // 
            // tbxDonvitinhtui
            // 
            this.tbxDonvitinhtui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxDonvitinhtui.Location = new System.Drawing.Point(15, 239);
            this.tbxDonvitinhtui.Name = "tbxDonvitinhtui";
            this.tbxDonvitinhtui.Size = new System.Drawing.Size(193, 20);
            this.tbxDonvitinhtui.TabIndex = 3;
            // 
            // tbxThongsokythuattui
            // 
            this.tbxThongsokythuattui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxThongsokythuattui.Location = new System.Drawing.Point(268, 87);
            this.tbxThongsokythuattui.Multiline = true;
            this.tbxThongsokythuattui.Name = "tbxThongsokythuattui";
            this.tbxThongsokythuattui.Size = new System.Drawing.Size(288, 96);
            this.tbxThongsokythuattui.TabIndex = 4;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FrmDanhmucTuiThem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.tbxThongsokythuattui);
            this.Controls.Add(this.tbxDonvitinhtui);
            this.Controls.Add(this.tbxTentui);
            this.Controls.Add(this.tbxMatui);
            this.Controls.Add(this.lblThongsokythuattui);
            this.Controls.Add(this.lblDonvitinh);
            this.Controls.Add(this.lblTentui);
            this.Controls.Add(this.lblMatui);
            this.Controls.Add(this.btnLuuthemtui);
            this.Controls.Add(this.btnThoatthemtui);
            this.Controls.Add(this.lblThemthongtintui);
            this.Controls.Add(this.lblDuongke);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucTuiThem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucTuiThem";
            this.Load += new System.EventHandler(this.FrmDanhmucTuiThem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnThoatthemtui;
        private System.Windows.Forms.Label lblThemthongtintui;
        private System.Windows.Forms.Label lblDuongke;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.Button btnLuuthemtui;
        private System.Windows.Forms.Label lblMatui;
        private System.Windows.Forms.Label lblTentui;
        private System.Windows.Forms.Label lblDonvitinh;
        private System.Windows.Forms.Label lblThongsokythuattui;
        private System.Windows.Forms.TextBox tbxMatui;
        private System.Windows.Forms.TextBox tbxTentui;
        private System.Windows.Forms.TextBox tbxDonvitinhtui;
        private System.Windows.Forms.TextBox tbxThongsokythuattui;
    }
}