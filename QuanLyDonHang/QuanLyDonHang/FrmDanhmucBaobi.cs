﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Quanlydonhang
{
    public partial class FrmDanhmucBaobi : Form
    {
        SqlConnection con = Database_Management.Connection;
        SqlDataAdapter adt = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        public FrmDanhmucBaobi()
        {
            InitializeComponent();
            this.dgvDanhmucbaobi.AutoGenerateColumns = false;
        }

        private void FrmDanhmucBaobi_Load(object sender, EventArgs e)
        {
            Database_Management.OpenSqlConnection();
            ds.Clear();
            cmd.CommandText = "select * from DanhmucBaobi";
            cmd.Connection = con;
            adt.SelectCommand = cmd;
            adt.Fill(ds, "DanhmucBaobi");
            dgvDanhmucbaobi.DataSource = ds;
            dgvDanhmucbaobi.DataMember = "DanhmucBaobi";
        }
    }
}
