﻿namespace Quanlydonhang
{
    partial class FrmDanhmucBaobiSua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucBaobiSua));
            this.btnLuusuabaobi = new System.Windows.Forms.Button();
            this.btnThoatsuabaobi = new System.Windows.Forms.Button();
            this.lblDanhmucbaobisua = new System.Windows.Forms.Label();
            this.lblDuongketui = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.tbxThongsokythuatbaobi = new System.Windows.Forms.TextBox();
            this.tbxDonvitinhbaobi = new System.Windows.Forms.TextBox();
            this.tbxTenbaobi = new System.Windows.Forms.TextBox();
            this.tbxMabaobi = new System.Windows.Forms.TextBox();
            this.lblThongsokythuatbaobi = new System.Windows.Forms.Label();
            this.lblDonvitinh = new System.Windows.Forms.Label();
            this.lblTenbaobi = new System.Windows.Forms.Label();
            this.lblMabaobi = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnLuusuabaobi
            // 
            this.btnLuusuabaobi.Location = new System.Drawing.Point(561, 19);
            this.btnLuusuabaobi.Margin = new System.Windows.Forms.Padding(2);
            this.btnLuusuabaobi.Name = "btnLuusuabaobi";
            this.btnLuusuabaobi.Size = new System.Drawing.Size(134, 28);
            this.btnLuusuabaobi.TabIndex = 29;
            this.btnLuusuabaobi.Text = "Lưu";
            this.btnLuusuabaobi.UseVisualStyleBackColor = true;
            // 
            // btnThoatsuabaobi
            // 
            this.btnThoatsuabaobi.BackColor = System.Drawing.SystemColors.Window;
            this.btnThoatsuabaobi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoatsuabaobi.Location = new System.Drawing.Point(713, 19);
            this.btnThoatsuabaobi.Margin = new System.Windows.Forms.Padding(2);
            this.btnThoatsuabaobi.Name = "btnThoatsuabaobi";
            this.btnThoatsuabaobi.Size = new System.Drawing.Size(134, 28);
            this.btnThoatsuabaobi.TabIndex = 31;
            this.btnThoatsuabaobi.Text = "Thoát";
            this.btnThoatsuabaobi.UseVisualStyleBackColor = false;
            // 
            // lblDanhmucbaobisua
            // 
            this.lblDanhmucbaobisua.AutoSize = true;
            this.lblDanhmucbaobisua.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmucbaobisua.Location = new System.Drawing.Point(7, 19);
            this.lblDanhmucbaobisua.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDanhmucbaobisua.Name = "lblDanhmucbaobisua";
            this.lblDanhmucbaobisua.Size = new System.Drawing.Size(211, 20);
            this.lblDanhmucbaobisua.TabIndex = 32;
            this.lblDanhmucbaobisua.Text = "SỬA THÔNG TIN BAO BÌ";
            // 
            // lblDuongketui
            // 
            this.lblDuongketui.AutoSize = true;
            this.lblDuongketui.ForeColor = System.Drawing.Color.DarkGray;
            this.lblDuongketui.Location = new System.Drawing.Point(8, 40);
            this.lblDuongketui.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDuongketui.Name = "lblDuongketui";
            this.lblDuongketui.Size = new System.Drawing.Size(865, 13);
            this.lblDuongketui.TabIndex = 30;
            this.lblDuongketui.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tbxThongsokythuatbaobi
            // 
            this.tbxThongsokythuatbaobi.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxThongsokythuatbaobi.Location = new System.Drawing.Point(268, 87);
            this.tbxThongsokythuatbaobi.Multiline = true;
            this.tbxThongsokythuatbaobi.Name = "tbxThongsokythuatbaobi";
            this.tbxThongsokythuatbaobi.Size = new System.Drawing.Size(288, 96);
            this.tbxThongsokythuatbaobi.TabIndex = 36;
            // 
            // tbxDonvitinhbaobi
            // 
            this.tbxDonvitinhbaobi.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxDonvitinhbaobi.Location = new System.Drawing.Point(15, 239);
            this.tbxDonvitinhbaobi.Name = "tbxDonvitinhbaobi";
            this.tbxDonvitinhbaobi.Size = new System.Drawing.Size(193, 20);
            this.tbxDonvitinhbaobi.TabIndex = 35;
            // 
            // tbxTenbaobi
            // 
            this.tbxTenbaobi.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxTenbaobi.Location = new System.Drawing.Point(15, 163);
            this.tbxTenbaobi.Name = "tbxTenbaobi";
            this.tbxTenbaobi.Size = new System.Drawing.Size(193, 20);
            this.tbxTenbaobi.TabIndex = 34;
            // 
            // tbxMabaobi
            // 
            this.tbxMabaobi.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxMabaobi.Location = new System.Drawing.Point(15, 87);
            this.tbxMabaobi.Name = "tbxMabaobi";
            this.tbxMabaobi.Size = new System.Drawing.Size(193, 20);
            this.tbxMabaobi.TabIndex = 33;
            // 
            // lblThongsokythuatbaobi
            // 
            this.lblThongsokythuatbaobi.AutoSize = true;
            this.lblThongsokythuatbaobi.Location = new System.Drawing.Point(265, 71);
            this.lblThongsokythuatbaobi.Name = "lblThongsokythuatbaobi";
            this.lblThongsokythuatbaobi.Size = new System.Drawing.Size(93, 13);
            this.lblThongsokythuatbaobi.TabIndex = 40;
            this.lblThongsokythuatbaobi.Text = "Thông số kỹ thuật";
            // 
            // lblDonvitinh
            // 
            this.lblDonvitinh.AutoSize = true;
            this.lblDonvitinh.Location = new System.Drawing.Point(13, 223);
            this.lblDonvitinh.Name = "lblDonvitinh";
            this.lblDonvitinh.Size = new System.Drawing.Size(60, 13);
            this.lblDonvitinh.TabIndex = 39;
            this.lblDonvitinh.Text = "Đơn vị tính";
            // 
            // lblTenbaobi
            // 
            this.lblTenbaobi.AutoSize = true;
            this.lblTenbaobi.Location = new System.Drawing.Point(13, 147);
            this.lblTenbaobi.Name = "lblTenbaobi";
            this.lblTenbaobi.Size = new System.Drawing.Size(58, 13);
            this.lblTenbaobi.TabIndex = 38;
            this.lblTenbaobi.Text = "Tên bao bì";
            // 
            // lblMabaobi
            // 
            this.lblMabaobi.AutoSize = true;
            this.lblMabaobi.Location = new System.Drawing.Point(13, 71);
            this.lblMabaobi.Name = "lblMabaobi";
            this.lblMabaobi.Size = new System.Drawing.Size(54, 13);
            this.lblMabaobi.TabIndex = 37;
            this.lblMabaobi.Text = "Mã bao bì";
            // 
            // FrmDanhmucBaobiSua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.tbxThongsokythuatbaobi);
            this.Controls.Add(this.tbxDonvitinhbaobi);
            this.Controls.Add(this.tbxTenbaobi);
            this.Controls.Add(this.tbxMabaobi);
            this.Controls.Add(this.lblThongsokythuatbaobi);
            this.Controls.Add(this.lblDonvitinh);
            this.Controls.Add(this.lblTenbaobi);
            this.Controls.Add(this.lblMabaobi);
            this.Controls.Add(this.btnLuusuabaobi);
            this.Controls.Add(this.btnThoatsuabaobi);
            this.Controls.Add(this.lblDanhmucbaobisua);
            this.Controls.Add(this.lblDuongketui);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucBaobiSua";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucBaobiSua";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnLuusuabaobi;
        private System.Windows.Forms.Button btnThoatsuabaobi;
        private System.Windows.Forms.Label lblDanhmucbaobisua;
        private System.Windows.Forms.Label lblDuongketui;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.TextBox tbxThongsokythuatbaobi;
        private System.Windows.Forms.TextBox tbxDonvitinhbaobi;
        private System.Windows.Forms.TextBox tbxTenbaobi;
        private System.Windows.Forms.TextBox tbxMabaobi;
        private System.Windows.Forms.Label lblThongsokythuatbaobi;
        private System.Windows.Forms.Label lblDonvitinh;
        private System.Windows.Forms.Label lblTenbaobi;
        private System.Windows.Forms.Label lblMabaobi;
    }
}