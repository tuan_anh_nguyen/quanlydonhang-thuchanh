﻿namespace Quanlydonhang
{
    partial class FrmDanhmucTemSua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucTemSua));
            this.tbxThongsokythuattem = new System.Windows.Forms.TextBox();
            this.tbxTentem = new System.Windows.Forms.TextBox();
            this.tbxMatem = new System.Windows.Forms.TextBox();
            this.lblThongsokythuat = new System.Windows.Forms.Label();
            this.lblDonvitinh = new System.Windows.Forms.Label();
            this.lblTentem = new System.Windows.Forms.Label();
            this.tbxDonvitinhtem = new System.Windows.Forms.TextBox();
            this.lblMatem = new System.Windows.Forms.Label();
            this.btnLuusuatem = new System.Windows.Forms.Button();
            this.btnThoatsuatem = new System.Windows.Forms.Button();
            this.lblDanhmuctemsua = new System.Windows.Forms.Label();
            this.lblDuongketem = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.SuspendLayout();
            // 
            // tbxThongsokythuattem
            // 
            this.tbxThongsokythuattem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxThongsokythuattem.Location = new System.Drawing.Point(268, 87);
            this.tbxThongsokythuattem.Multiline = true;
            this.tbxThongsokythuattem.Name = "tbxThongsokythuattem";
            this.tbxThongsokythuattem.Size = new System.Drawing.Size(288, 96);
            this.tbxThongsokythuattem.TabIndex = 28;
            // 
            // tbxTentem
            // 
            this.tbxTentem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxTentem.Location = new System.Drawing.Point(15, 163);
            this.tbxTentem.Name = "tbxTentem";
            this.tbxTentem.Size = new System.Drawing.Size(193, 20);
            this.tbxTentem.TabIndex = 26;
            // 
            // tbxMatem
            // 
            this.tbxMatem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxMatem.Location = new System.Drawing.Point(15, 87);
            this.tbxMatem.Name = "tbxMatem";
            this.tbxMatem.Size = new System.Drawing.Size(193, 20);
            this.tbxMatem.TabIndex = 25;
            // 
            // lblThongsokythuat
            // 
            this.lblThongsokythuat.AutoSize = true;
            this.lblThongsokythuat.Location = new System.Drawing.Point(265, 71);
            this.lblThongsokythuat.Name = "lblThongsokythuat";
            this.lblThongsokythuat.Size = new System.Drawing.Size(93, 13);
            this.lblThongsokythuat.TabIndex = 36;
            this.lblThongsokythuat.Text = "Thông số kỹ thuật";
            // 
            // lblDonvitinh
            // 
            this.lblDonvitinh.AutoSize = true;
            this.lblDonvitinh.Location = new System.Drawing.Point(13, 223);
            this.lblDonvitinh.Name = "lblDonvitinh";
            this.lblDonvitinh.Size = new System.Drawing.Size(60, 13);
            this.lblDonvitinh.TabIndex = 35;
            this.lblDonvitinh.Text = "Đơn vị tính";
            // 
            // lblTentem
            // 
            this.lblTentem.AutoSize = true;
            this.lblTentem.Location = new System.Drawing.Point(13, 147);
            this.lblTentem.Name = "lblTentem";
            this.lblTentem.Size = new System.Drawing.Size(46, 13);
            this.lblTentem.TabIndex = 34;
            this.lblTentem.Text = "Tên tem";
            // 
            // tbxDonvitinhtem
            // 
            this.tbxDonvitinhtem.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxDonvitinhtem.Location = new System.Drawing.Point(15, 239);
            this.tbxDonvitinhtem.Name = "tbxDonvitinhtem";
            this.tbxDonvitinhtem.Size = new System.Drawing.Size(193, 20);
            this.tbxDonvitinhtem.TabIndex = 27;
            // 
            // lblMatem
            // 
            this.lblMatem.AutoSize = true;
            this.lblMatem.Location = new System.Drawing.Point(13, 71);
            this.lblMatem.Name = "lblMatem";
            this.lblMatem.Size = new System.Drawing.Size(42, 13);
            this.lblMatem.TabIndex = 33;
            this.lblMatem.Text = "Mã tem";
            // 
            // btnLuusuatem
            // 
            this.btnLuusuatem.Location = new System.Drawing.Point(561, 19);
            this.btnLuusuatem.Margin = new System.Windows.Forms.Padding(2);
            this.btnLuusuatem.Name = "btnLuusuatem";
            this.btnLuusuatem.Size = new System.Drawing.Size(134, 28);
            this.btnLuusuatem.TabIndex = 29;
            this.btnLuusuatem.Text = "Lưu";
            this.btnLuusuatem.UseVisualStyleBackColor = true;
            // 
            // btnThoatsuatem
            // 
            this.btnThoatsuatem.BackColor = System.Drawing.SystemColors.Window;
            this.btnThoatsuatem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoatsuatem.Location = new System.Drawing.Point(713, 19);
            this.btnThoatsuatem.Margin = new System.Windows.Forms.Padding(2);
            this.btnThoatsuatem.Name = "btnThoatsuatem";
            this.btnThoatsuatem.Size = new System.Drawing.Size(134, 28);
            this.btnThoatsuatem.TabIndex = 31;
            this.btnThoatsuatem.Text = "Thoát";
            this.btnThoatsuatem.UseVisualStyleBackColor = false;
            // 
            // lblDanhmuctemsua
            // 
            this.lblDanhmuctemsua.AutoSize = true;
            this.lblDanhmuctemsua.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmuctemsua.Location = new System.Drawing.Point(7, 19);
            this.lblDanhmuctemsua.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDanhmuctemsua.Name = "lblDanhmuctemsua";
            this.lblDanhmuctemsua.Size = new System.Drawing.Size(187, 20);
            this.lblDanhmuctemsua.TabIndex = 32;
            this.lblDanhmuctemsua.Text = "SỬA THÔNG TIN TEM";
            // 
            // lblDuongketem
            // 
            this.lblDuongketem.AutoSize = true;
            this.lblDuongketem.ForeColor = System.Drawing.Color.DarkGray;
            this.lblDuongketem.Location = new System.Drawing.Point(8, 40);
            this.lblDuongketem.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDuongketem.Name = "lblDuongketem";
            this.lblDuongketem.Size = new System.Drawing.Size(865, 13);
            this.lblDuongketem.TabIndex = 30;
            this.lblDuongketem.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FrmDanhmucTemSua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(874, 417);
            this.Controls.Add(this.tbxThongsokythuattem);
            this.Controls.Add(this.tbxTentem);
            this.Controls.Add(this.tbxMatem);
            this.Controls.Add(this.lblThongsokythuat);
            this.Controls.Add(this.lblDonvitinh);
            this.Controls.Add(this.lblTentem);
            this.Controls.Add(this.tbxDonvitinhtem);
            this.Controls.Add(this.lblMatem);
            this.Controls.Add(this.btnLuusuatem);
            this.Controls.Add(this.btnThoatsuatem);
            this.Controls.Add(this.lblDanhmuctemsua);
            this.Controls.Add(this.lblDuongketem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucTemSua";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucTemSua";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxThongsokythuattem;
        private System.Windows.Forms.TextBox tbxTentem;
        private System.Windows.Forms.TextBox tbxMatem;
        private System.Windows.Forms.Label lblThongsokythuat;
        private System.Windows.Forms.Label lblDonvitinh;
        private System.Windows.Forms.Label lblTentem;
        private System.Windows.Forms.TextBox tbxDonvitinhtem;
        private System.Windows.Forms.Label lblMatem;
        private System.Windows.Forms.Button btnLuusuatem;
        private System.Windows.Forms.Button btnThoatsuatem;
        private System.Windows.Forms.Label lblDanhmuctemsua;
        private System.Windows.Forms.Label lblDuongketem;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
    }
}