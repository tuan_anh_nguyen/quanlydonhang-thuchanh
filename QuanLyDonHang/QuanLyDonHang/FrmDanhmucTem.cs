﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Quanlydonhang
{
    public partial class FrmDanhmucTem : Form
    {
        SqlConnection con = Database_Management.Connection;
        SqlDataAdapter adt = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        public FrmDanhmucTem()
        {
            InitializeComponent();
            this.dgvDanhmuctem.AutoGenerateColumns = false;
        }

        private void FrmDanhmucTem_Load(object sender, EventArgs e)
        {
            Database_Management.OpenSqlConnection();
            ds.Clear();
            cmd.CommandText = "select * from DanhmucTem";
            cmd.Connection = con;
            adt.SelectCommand = cmd;
            adt.Fill(ds, "DanhmucTem");
            dgvDanhmuctem.DataSource = ds;
            dgvDanhmuctem.DataMember = "DanhmucTem";
        }
    }
}
