﻿namespace Quanlydonhang
{
    partial class FrmDanhmucTui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucTui));
            this.dgvDanhmuctui = new System.Windows.Forms.DataGridView();
            this.Matui = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenLoaiTui = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DonViTinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThongSoKyThuat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suadanhmuctui = new System.Windows.Forms.DataGridViewImageColumn();
            this.XoaDanhmuctui = new System.Windows.Forms.DataGridViewImageColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDanhmuctui = new System.Windows.Forms.Label();
            this.btnThemloaitui = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhmuctui)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDanhmuctui
            // 
            this.dgvDanhmuctui.AllowUserToAddRows = false;
            this.dgvDanhmuctui.AllowUserToDeleteRows = false;
            this.dgvDanhmuctui.AllowUserToResizeColumns = false;
            this.dgvDanhmuctui.AllowUserToResizeRows = false;
            this.dgvDanhmuctui.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvDanhmuctui.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDanhmuctui.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.dgvDanhmuctui.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDanhmuctui.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDanhmuctui.ColumnHeadersHeight = 35;
            this.dgvDanhmuctui.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Matui,
            this.TenLoaiTui,
            this.DonViTinh,
            this.ThongSoKyThuat,
            this.Suadanhmuctui,
            this.XoaDanhmuctui});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDanhmuctui.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDanhmuctui.EnableHeadersVisualStyles = false;
            this.dgvDanhmuctui.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDanhmuctui.Location = new System.Drawing.Point(9, 53);
            this.dgvDanhmuctui.Margin = new System.Windows.Forms.Padding(2);
            this.dgvDanhmuctui.Name = "dgvDanhmuctui";
            this.dgvDanhmuctui.ReadOnly = true;
            this.dgvDanhmuctui.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvDanhmuctui.RowHeadersVisible = false;
            this.dgvDanhmuctui.RowTemplate.Height = 35;
            this.dgvDanhmuctui.Size = new System.Drawing.Size(872, 392);
            this.dgvDanhmuctui.TabIndex = 0;
            // 
            // Matui
            // 
            this.Matui.DataPropertyName = "MaTui";
            this.Matui.HeaderText = "Mã túi";
            this.Matui.Name = "Matui";
            this.Matui.ReadOnly = true;
            this.Matui.Width = 150;
            // 
            // TenLoaiTui
            // 
            this.TenLoaiTui.DataPropertyName = "TenLoaiTui";
            this.TenLoaiTui.HeaderText = "Tên Loại Túi";
            this.TenLoaiTui.Name = "TenLoaiTui";
            this.TenLoaiTui.ReadOnly = true;
            this.TenLoaiTui.Width = 150;
            // 
            // DonViTinh
            // 
            this.DonViTinh.DataPropertyName = "DonViTinh";
            this.DonViTinh.HeaderText = "Đơn vị tính";
            this.DonViTinh.Name = "DonViTinh";
            this.DonViTinh.ReadOnly = true;
            this.DonViTinh.Width = 150;
            // 
            // ThongSoKyThuat
            // 
            this.ThongSoKyThuat.DataPropertyName = "ThongSoKyThuat";
            this.ThongSoKyThuat.HeaderText = "Thông số kỹ thuật";
            this.ThongSoKyThuat.Name = "ThongSoKyThuat";
            this.ThongSoKyThuat.ReadOnly = true;
            this.ThongSoKyThuat.Width = 250;
            // 
            // Suadanhmuctui
            // 
            this.Suadanhmuctui.HeaderText = "";
            this.Suadanhmuctui.Image = global::Quanlydonhang.Properties.Resources.Edit_Icon;
            this.Suadanhmuctui.Name = "Suadanhmuctui";
            this.Suadanhmuctui.ReadOnly = true;
            this.Suadanhmuctui.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Suadanhmuctui.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Suadanhmuctui.Width = 80;
            // 
            // XoaDanhmuctui
            // 
            this.XoaDanhmuctui.HeaderText = "";
            this.XoaDanhmuctui.Image = global::Quanlydonhang.Properties.Resources.Delete_Icon;
            this.XoaDanhmuctui.Name = "XoaDanhmuctui";
            this.XoaDanhmuctui.ReadOnly = true;
            this.XoaDanhmuctui.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.XoaDanhmuctui.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.XoaDanhmuctui.Width = 80;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkGray;
            this.label1.Location = new System.Drawing.Point(7, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(865, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // lblDanhmuctui
            // 
            this.lblDanhmuctui.AutoSize = true;
            this.lblDanhmuctui.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmuctui.Location = new System.Drawing.Point(6, 19);
            this.lblDanhmuctui.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDanhmuctui.Name = "lblDanhmuctui";
            this.lblDanhmuctui.Size = new System.Drawing.Size(137, 20);
            this.lblDanhmuctui.TabIndex = 2;
            this.lblDanhmuctui.Text = "DANH MỤC TÚI";
            // 
            // btnThemloaitui
            // 
            this.btnThemloaitui.Location = new System.Drawing.Point(712, 19);
            this.btnThemloaitui.Margin = new System.Windows.Forms.Padding(2);
            this.btnThemloaitui.Name = "btnThemloaitui";
            this.btnThemloaitui.Size = new System.Drawing.Size(134, 28);
            this.btnThemloaitui.TabIndex = 3;
            this.btnThemloaitui.Text = "Thêm loại túi";
            this.btnThemloaitui.UseVisualStyleBackColor = true;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FrmDanhmucTui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.btnThemloaitui);
            this.Controls.Add(this.lblDanhmuctui);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvDanhmuctui);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmDanhmucTui";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DanhmucTui";
            this.Load += new System.EventHandler(this.FrmDanhmucTui_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhmuctui)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDanhmuctui;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDanhmuctui;
        private System.Windows.Forms.Button btnThemloaitui;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Matui;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenLoaiTui;
        private System.Windows.Forms.DataGridViewTextBoxColumn DonViTinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThongSoKyThuat;
        private System.Windows.Forms.DataGridViewImageColumn Suadanhmuctui;
        private System.Windows.Forms.DataGridViewImageColumn XoaDanhmuctui;
    }
}