﻿namespace Quanlydonhang
{
    partial class FrmDanhmucTuiSua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucTuiSua));
            this.tbxThongsokythuattui = new System.Windows.Forms.TextBox();
            this.tbxTentui = new System.Windows.Forms.TextBox();
            this.tbxMatui = new System.Windows.Forms.TextBox();
            this.lblThongsokythuattui = new System.Windows.Forms.Label();
            this.lblDonvitinh = new System.Windows.Forms.Label();
            this.lblTentui = new System.Windows.Forms.Label();
            this.tbxDonvitinhtui = new System.Windows.Forms.TextBox();
            this.lblMatui = new System.Windows.Forms.Label();
            this.btnLuusuatui = new System.Windows.Forms.Button();
            this.btnThoatsuatui = new System.Windows.Forms.Button();
            this.lblDanhmuctuisua = new System.Windows.Forms.Label();
            this.lblDuongketui = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.SuspendLayout();
            // 
            // tbxThongsokythuattui
            // 
            this.tbxThongsokythuattui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxThongsokythuattui.Location = new System.Drawing.Point(268, 87);
            this.tbxThongsokythuattui.Multiline = true;
            this.tbxThongsokythuattui.Name = "tbxThongsokythuattui";
            this.tbxThongsokythuattui.Size = new System.Drawing.Size(288, 96);
            this.tbxThongsokythuattui.TabIndex = 16;
            // 
            // tbxTentui
            // 
            this.tbxTentui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxTentui.Location = new System.Drawing.Point(15, 163);
            this.tbxTentui.Name = "tbxTentui";
            this.tbxTentui.Size = new System.Drawing.Size(193, 20);
            this.tbxTentui.TabIndex = 14;
            // 
            // tbxMatui
            // 
            this.tbxMatui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxMatui.Location = new System.Drawing.Point(15, 87);
            this.tbxMatui.Name = "tbxMatui";
            this.tbxMatui.Size = new System.Drawing.Size(193, 20);
            this.tbxMatui.TabIndex = 13;
            // 
            // lblThongsokythuattui
            // 
            this.lblThongsokythuattui.AutoSize = true;
            this.lblThongsokythuattui.Location = new System.Drawing.Point(265, 71);
            this.lblThongsokythuattui.Name = "lblThongsokythuattui";
            this.lblThongsokythuattui.Size = new System.Drawing.Size(93, 13);
            this.lblThongsokythuattui.TabIndex = 24;
            this.lblThongsokythuattui.Text = "Thông số kỹ thuật";
            // 
            // lblDonvitinh
            // 
            this.lblDonvitinh.AutoSize = true;
            this.lblDonvitinh.Location = new System.Drawing.Point(13, 223);
            this.lblDonvitinh.Name = "lblDonvitinh";
            this.lblDonvitinh.Size = new System.Drawing.Size(60, 13);
            this.lblDonvitinh.TabIndex = 23;
            this.lblDonvitinh.Text = "Đơn vị tính";
            // 
            // lblTentui
            // 
            this.lblTentui.AutoSize = true;
            this.lblTentui.Location = new System.Drawing.Point(13, 147);
            this.lblTentui.Name = "lblTentui";
            this.lblTentui.Size = new System.Drawing.Size(40, 13);
            this.lblTentui.TabIndex = 22;
            this.lblTentui.Text = "Tên túi";
            // 
            // tbxDonvitinhtui
            // 
            this.tbxDonvitinhtui.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tbxDonvitinhtui.Location = new System.Drawing.Point(15, 239);
            this.tbxDonvitinhtui.Name = "tbxDonvitinhtui";
            this.tbxDonvitinhtui.Size = new System.Drawing.Size(193, 20);
            this.tbxDonvitinhtui.TabIndex = 15;
            // 
            // lblMatui
            // 
            this.lblMatui.AutoSize = true;
            this.lblMatui.Location = new System.Drawing.Point(13, 71);
            this.lblMatui.Name = "lblMatui";
            this.lblMatui.Size = new System.Drawing.Size(36, 13);
            this.lblMatui.TabIndex = 21;
            this.lblMatui.Text = "Mã túi";
            // 
            // btnLuusuatui
            // 
            this.btnLuusuatui.Location = new System.Drawing.Point(561, 19);
            this.btnLuusuatui.Margin = new System.Windows.Forms.Padding(2);
            this.btnLuusuatui.Name = "btnLuusuatui";
            this.btnLuusuatui.Size = new System.Drawing.Size(134, 28);
            this.btnLuusuatui.TabIndex = 17;
            this.btnLuusuatui.Text = "Lưu";
            this.btnLuusuatui.UseVisualStyleBackColor = true;
            // 
            // btnThoatsuatui
            // 
            this.btnThoatsuatui.BackColor = System.Drawing.SystemColors.Window;
            this.btnThoatsuatui.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoatsuatui.Location = new System.Drawing.Point(713, 19);
            this.btnThoatsuatui.Margin = new System.Windows.Forms.Padding(2);
            this.btnThoatsuatui.Name = "btnThoatsuatui";
            this.btnThoatsuatui.Size = new System.Drawing.Size(134, 28);
            this.btnThoatsuatui.TabIndex = 19;
            this.btnThoatsuatui.Text = "Thoát";
            this.btnThoatsuatui.UseVisualStyleBackColor = false;
            // 
            // lblDanhmuctuisua
            // 
            this.lblDanhmuctuisua.AutoSize = true;
            this.lblDanhmuctuisua.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmuctuisua.Location = new System.Drawing.Point(7, 19);
            this.lblDanhmuctuisua.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDanhmuctuisua.Name = "lblDanhmuctuisua";
            this.lblDanhmuctuisua.Size = new System.Drawing.Size(180, 20);
            this.lblDanhmuctuisua.TabIndex = 20;
            this.lblDanhmuctuisua.Text = "SỬA THÔNG TIN TÚI";
            // 
            // lblDuongketui
            // 
            this.lblDuongketui.AutoSize = true;
            this.lblDuongketui.ForeColor = System.Drawing.Color.DarkGray;
            this.lblDuongketui.Location = new System.Drawing.Point(8, 40);
            this.lblDuongketui.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDuongketui.Name = "lblDuongketui";
            this.lblDuongketui.Size = new System.Drawing.Size(865, 13);
            this.lblDuongketui.TabIndex = 18;
            this.lblDuongketui.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FrmDanhmucTuiSua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.tbxThongsokythuattui);
            this.Controls.Add(this.tbxTentui);
            this.Controls.Add(this.tbxMatui);
            this.Controls.Add(this.lblThongsokythuattui);
            this.Controls.Add(this.lblDonvitinh);
            this.Controls.Add(this.lblTentui);
            this.Controls.Add(this.tbxDonvitinhtui);
            this.Controls.Add(this.lblMatui);
            this.Controls.Add(this.btnLuusuatui);
            this.Controls.Add(this.btnThoatsuatui);
            this.Controls.Add(this.lblDanhmuctuisua);
            this.Controls.Add(this.lblDuongketui);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucTuiSua";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucTuiSua";
            this.Load += new System.EventHandler(this.FrmDanhmucTuiSua_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxThongsokythuattui;
        private System.Windows.Forms.TextBox tbxTentui;
        private System.Windows.Forms.TextBox tbxMatui;
        private System.Windows.Forms.Label lblThongsokythuattui;
        private System.Windows.Forms.Label lblDonvitinh;
        private System.Windows.Forms.Label lblTentui;
        private System.Windows.Forms.TextBox tbxDonvitinhtui;
        private System.Windows.Forms.Label lblMatui;
        private System.Windows.Forms.Button btnLuusuatui;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Button btnThoatsuatui;
        private System.Windows.Forms.Label lblDanhmuctuisua;
        private System.Windows.Forms.Label lblDuongketui;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
    }
}