﻿namespace Quanlydonhang
{
    partial class FrmDanhmucBaobi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucBaobi));
            this.dgvDanhmucbaobi = new System.Windows.Forms.DataGridView();
            this.btnThemloaibaobi = new System.Windows.Forms.Button();
            this.lblDanhmucbaobi = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.MaBaoBi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenBaoBi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DonViTinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThongSoKyThuat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suadanhmuctui = new System.Windows.Forms.DataGridViewImageColumn();
            this.XoaDanhmuctui = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhmucbaobi)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDanhmucbaobi
            // 
            this.dgvDanhmucbaobi.AllowUserToAddRows = false;
            this.dgvDanhmucbaobi.AllowUserToDeleteRows = false;
            this.dgvDanhmucbaobi.AllowUserToResizeColumns = false;
            this.dgvDanhmucbaobi.AllowUserToResizeRows = false;
            this.dgvDanhmucbaobi.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvDanhmucbaobi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDanhmucbaobi.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.dgvDanhmucbaobi.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDanhmucbaobi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDanhmucbaobi.ColumnHeadersHeight = 35;
            this.dgvDanhmucbaobi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaBaoBi,
            this.TenBaoBi,
            this.DonViTinh,
            this.ThongSoKyThuat,
            this.Suadanhmuctui,
            this.XoaDanhmuctui});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDanhmucbaobi.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDanhmucbaobi.EnableHeadersVisualStyles = false;
            this.dgvDanhmucbaobi.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDanhmucbaobi.Location = new System.Drawing.Point(9, 53);
            this.dgvDanhmucbaobi.Margin = new System.Windows.Forms.Padding(2);
            this.dgvDanhmucbaobi.Name = "dgvDanhmucbaobi";
            this.dgvDanhmucbaobi.ReadOnly = true;
            this.dgvDanhmucbaobi.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvDanhmucbaobi.RowHeadersVisible = false;
            this.dgvDanhmucbaobi.RowTemplate.Height = 35;
            this.dgvDanhmucbaobi.Size = new System.Drawing.Size(872, 392);
            this.dgvDanhmucbaobi.TabIndex = 4;
            // 
            // btnThemloaibaobi
            // 
            this.btnThemloaibaobi.Location = new System.Drawing.Point(712, 19);
            this.btnThemloaibaobi.Margin = new System.Windows.Forms.Padding(2);
            this.btnThemloaibaobi.Name = "btnThemloaibaobi";
            this.btnThemloaibaobi.Size = new System.Drawing.Size(134, 28);
            this.btnThemloaibaobi.TabIndex = 7;
            this.btnThemloaibaobi.Text = "Thêm loại bao bì";
            this.btnThemloaibaobi.UseVisualStyleBackColor = true;
            // 
            // lblDanhmucbaobi
            // 
            this.lblDanhmucbaobi.AutoSize = true;
            this.lblDanhmucbaobi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmucbaobi.Location = new System.Drawing.Point(6, 19);
            this.lblDanhmucbaobi.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDanhmucbaobi.Name = "lblDanhmucbaobi";
            this.lblDanhmucbaobi.Size = new System.Drawing.Size(168, 20);
            this.lblDanhmucbaobi.TabIndex = 6;
            this.lblDanhmucbaobi.Text = "DANH MỤC BAO BÌ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkGray;
            this.label1.Location = new System.Drawing.Point(7, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(865, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 80;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn2.Width = 80;
            // 
            // MaBaoBi
            // 
            this.MaBaoBi.DataPropertyName = "MaBaoBi";
            this.MaBaoBi.HeaderText = "Mã Bao Bì";
            this.MaBaoBi.Name = "MaBaoBi";
            this.MaBaoBi.ReadOnly = true;
            this.MaBaoBi.Width = 150;
            // 
            // TenBaoBi
            // 
            this.TenBaoBi.DataPropertyName = "TenBaoBi";
            this.TenBaoBi.HeaderText = "Tên Loại Bao bì";
            this.TenBaoBi.Name = "TenBaoBi";
            this.TenBaoBi.ReadOnly = true;
            this.TenBaoBi.Width = 150;
            // 
            // DonViTinh
            // 
            this.DonViTinh.DataPropertyName = "DonViTinh";
            this.DonViTinh.HeaderText = "Đơn vị tính";
            this.DonViTinh.Name = "DonViTinh";
            this.DonViTinh.ReadOnly = true;
            this.DonViTinh.Width = 150;
            // 
            // ThongSoKyThuat
            // 
            this.ThongSoKyThuat.DataPropertyName = "ThongSoKyThuat";
            this.ThongSoKyThuat.HeaderText = "Thông số kỹ thuật";
            this.ThongSoKyThuat.Name = "ThongSoKyThuat";
            this.ThongSoKyThuat.ReadOnly = true;
            this.ThongSoKyThuat.Width = 250;
            // 
            // Suadanhmuctui
            // 
            this.Suadanhmuctui.HeaderText = "";
            this.Suadanhmuctui.Image = global::Quanlydonhang.Properties.Resources.Edit_Icon;
            this.Suadanhmuctui.Name = "Suadanhmuctui";
            this.Suadanhmuctui.ReadOnly = true;
            this.Suadanhmuctui.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Suadanhmuctui.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Suadanhmuctui.Width = 80;
            // 
            // XoaDanhmuctui
            // 
            this.XoaDanhmuctui.HeaderText = "";
            this.XoaDanhmuctui.Image = global::Quanlydonhang.Properties.Resources.Delete_Icon;
            this.XoaDanhmuctui.Name = "XoaDanhmuctui";
            this.XoaDanhmuctui.ReadOnly = true;
            this.XoaDanhmuctui.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.XoaDanhmuctui.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.XoaDanhmuctui.Width = 80;
            // 
            // FrmDanhmucBaobi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.dgvDanhmucbaobi);
            this.Controls.Add(this.btnThemloaibaobi);
            this.Controls.Add(this.lblDanhmucbaobi);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucBaobi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucBaobi";
            this.Load += new System.EventHandler(this.FrmDanhmucBaobi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhmucbaobi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDanhmucbaobi;
        private System.Windows.Forms.Button btnThemloaibaobi;
        private System.Windows.Forms.Label lblDanhmucbaobi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaBaoBi;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenBaoBi;
        private System.Windows.Forms.DataGridViewTextBoxColumn DonViTinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThongSoKyThuat;
        private System.Windows.Forms.DataGridViewImageColumn Suadanhmuctui;
        private System.Windows.Forms.DataGridViewImageColumn XoaDanhmuctui;
    }
}