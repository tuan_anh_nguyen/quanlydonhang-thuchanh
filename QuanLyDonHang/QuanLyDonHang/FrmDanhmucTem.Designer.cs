﻿namespace Quanlydonhang
{
    partial class FrmDanhmucTem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDanhmucTem));
            this.btnThemloaitem = new System.Windows.Forms.Button();
            this.lblDanhmuctem = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDanhmuctem = new System.Windows.Forms.DataGridView();
            this.MaTem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenLoaiTem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DonViTinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThongSoKyThuat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suadanhmuctui = new System.Windows.Forms.DataGridViewImageColumn();
            this.XoaDanhmuctui = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhmuctem)).BeginInit();
            this.SuspendLayout();
            // 
            // btnThemloaitem
            // 
            this.btnThemloaitem.Location = new System.Drawing.Point(712, 19);
            this.btnThemloaitem.Margin = new System.Windows.Forms.Padding(2);
            this.btnThemloaitem.Name = "btnThemloaitem";
            this.btnThemloaitem.Size = new System.Drawing.Size(134, 28);
            this.btnThemloaitem.TabIndex = 7;
            this.btnThemloaitem.Text = "Thêm Loại Tem";
            this.btnThemloaitem.UseVisualStyleBackColor = true;
            // 
            // lblDanhmuctem
            // 
            this.lblDanhmuctem.AutoSize = true;
            this.lblDanhmuctem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmuctem.Location = new System.Drawing.Point(6, 19);
            this.lblDanhmuctem.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDanhmuctem.Name = "lblDanhmuctem";
            this.lblDanhmuctem.Size = new System.Drawing.Size(144, 20);
            this.lblDanhmuctem.TabIndex = 6;
            this.lblDanhmuctem.Text = "DANH MỤC TEM";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkGray;
            this.label1.Location = new System.Drawing.Point(7, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(865, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "_________________________________________________________________________________" +
    "______________________________________________________________";
            // 
            // dgvDanhmuctem
            // 
            this.dgvDanhmuctem.AllowUserToAddRows = false;
            this.dgvDanhmuctem.AllowUserToDeleteRows = false;
            this.dgvDanhmuctem.AllowUserToOrderColumns = true;
            this.dgvDanhmuctem.AllowUserToResizeColumns = false;
            this.dgvDanhmuctem.AllowUserToResizeRows = false;
            this.dgvDanhmuctem.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvDanhmuctem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDanhmuctem.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.dgvDanhmuctem.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDanhmuctem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDanhmuctem.ColumnHeadersHeight = 35;
            this.dgvDanhmuctem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaTem,
            this.TenLoaiTem,
            this.DonViTinh,
            this.ThongSoKyThuat,
            this.Suadanhmuctui,
            this.XoaDanhmuctui});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDanhmuctem.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDanhmuctem.EnableHeadersVisualStyles = false;
            this.dgvDanhmuctem.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDanhmuctem.Location = new System.Drawing.Point(9, 53);
            this.dgvDanhmuctem.Margin = new System.Windows.Forms.Padding(2);
            this.dgvDanhmuctem.Name = "dgvDanhmuctem";
            this.dgvDanhmuctem.ReadOnly = true;
            this.dgvDanhmuctem.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvDanhmuctem.RowHeadersVisible = false;
            this.dgvDanhmuctem.RowTemplate.Height = 35;
            this.dgvDanhmuctem.Size = new System.Drawing.Size(872, 392);
            this.dgvDanhmuctem.TabIndex = 4;
            // 
            // MaTem
            // 
            this.MaTem.DataPropertyName = "MaTem";
            this.MaTem.HeaderText = "Mã Tem";
            this.MaTem.Name = "MaTem";
            this.MaTem.ReadOnly = true;
            this.MaTem.Width = 150;
            // 
            // TenLoaiTem
            // 
            this.TenLoaiTem.DataPropertyName = "TenLoaiTem";
            this.TenLoaiTem.HeaderText = "Tên Loại Tem";
            this.TenLoaiTem.Name = "TenLoaiTem";
            this.TenLoaiTem.ReadOnly = true;
            this.TenLoaiTem.Width = 150;
            // 
            // DonViTinh
            // 
            this.DonViTinh.DataPropertyName = "DonViTinh";
            this.DonViTinh.HeaderText = "Đơn Vị Tính";
            this.DonViTinh.Name = "DonViTinh";
            this.DonViTinh.ReadOnly = true;
            this.DonViTinh.Width = 150;
            // 
            // ThongSoKyThuat
            // 
            this.ThongSoKyThuat.DataPropertyName = "ThongSoKyThuat";
            this.ThongSoKyThuat.HeaderText = "Thông Số Kỹ Thuật";
            this.ThongSoKyThuat.Name = "ThongSoKyThuat";
            this.ThongSoKyThuat.ReadOnly = true;
            this.ThongSoKyThuat.Width = 250;
            // 
            // Suadanhmuctui
            // 
            this.Suadanhmuctui.HeaderText = "";
            this.Suadanhmuctui.Image = global::Quanlydonhang.Properties.Resources.Edit_Icon;
            this.Suadanhmuctui.Name = "Suadanhmuctui";
            this.Suadanhmuctui.ReadOnly = true;
            this.Suadanhmuctui.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Suadanhmuctui.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Suadanhmuctui.Width = 80;
            // 
            // XoaDanhmuctui
            // 
            this.XoaDanhmuctui.HeaderText = "";
            this.XoaDanhmuctui.Image = global::Quanlydonhang.Properties.Resources.Delete_Icon;
            this.XoaDanhmuctui.Name = "XoaDanhmuctui";
            this.XoaDanhmuctui.ReadOnly = true;
            this.XoaDanhmuctui.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.XoaDanhmuctui.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.XoaDanhmuctui.Width = 80;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FrmDanhmucTem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(890, 456);
            this.Controls.Add(this.btnThemloaitem);
            this.Controls.Add(this.lblDanhmuctem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvDanhmuctem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDanhmucTem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDanhmucTem";
            this.Load += new System.EventHandler(this.FrmDanhmucTem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhmuctem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnThemloaitem;
        private System.Windows.Forms.Label lblDanhmuctem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridView dgvDanhmuctem;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTem;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenLoaiTem;
        private System.Windows.Forms.DataGridViewTextBoxColumn DonViTinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThongSoKyThuat;
        private System.Windows.Forms.DataGridViewImageColumn Suadanhmuctui;
        private System.Windows.Forms.DataGridViewImageColumn XoaDanhmuctui;
    }
}