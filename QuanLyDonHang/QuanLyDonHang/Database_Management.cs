﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Quanlydonhang
{
    class Database_Management
    {
        public static SqlConnection Connection = new SqlConnection();
        public static void OpenSqlConnection()
        {
            if (Connection.State == System.Data.ConnectionState.Closed)
            {
                string s = @"Data Source = IT\SQLEXPRESS; Initial Catalog = QLDH; " + "Integrated Security = true; ";
                Connection.ConnectionString = s;
                Connection.Open();
            }
        }
    }
}
